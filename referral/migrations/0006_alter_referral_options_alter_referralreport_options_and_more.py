# Generated by Django 4.1.1 on 2022-11-07 11:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('referral', '0005_referredperson_referred_person_email'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='referral',
            options={'verbose_name': '  Referral', 'verbose_name_plural': '  Referrals'},
        ),
        migrations.AlterModelOptions(
            name='referralreport',
            options={'verbose_name': 'Referral Report', 'verbose_name_plural': 'Referral Report'},
        ),
        migrations.AlterModelOptions(
            name='referredperson',
            options={'verbose_name': ' Referred Person', 'verbose_name_plural': ' Referred Person'},
        ),
        migrations.RemoveField(
            model_name='referral',
            name='total_referral',
        ),
        migrations.AlterField(
            model_name='referral',
            name='referral_discount',
            field=models.IntegerField(default=0),
        ),
    ]
